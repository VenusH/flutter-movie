import 'package:flutter/material.dart';
import 'package:flutter_application_1/widgets/movie_card.dart';

class MovieCardContainer extends StatelessWidget {
  final Color themeColor;
  final ScrollController scrollController;
  final List<MovieCard> movieCards;

  MovieCardContainer({
    required this.themeColor,
    required this.scrollController,
    required this.movieCards,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      //movie_card_container
      child: Padding(
        padding: EdgeInsets.only(right: 20, left: 20),
        child: SingleChildScrollView(
          controller: scrollController,
          child: Padding(
            padding: EdgeInsets.all(2.5),
            child: Wrap(children: movieCards),
          ),
        ),
      ),
    );
  }
}
