import 'package:flutter/material.dart';
import 'package:flutter_application_1/models/movie_preview.dart';
import 'package:flutter_application_1/screens/details_screen.dart';
import 'package:flutter_application_1/utils/star_calculator.dart'
    as starCalculator;

class MovieCard extends StatelessWidget {
  final MoviePreview moviePreview;
  //final Color themeColor;
  final int? contentLoadedFromPage;

  MovieCard({
    required this.moviePreview,
    this.contentLoadedFromPage,
  });

  @override
  Widget build(BuildContext context) {
    List<Widget> stars =
        starCalculator.getStars(rating: moviePreview.rating, starSize: 2.0);

    return Container(
            width: MediaQuery.of(context).size.width * 0.6,
            child: Card(
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) {
                        return DetailsScreen(
                          id: moviePreview.id,
                        );
                      },
                    ),
                  );
                },
                child: FadeInImage.assetNetwork(
                    placeholder: 'assets/images/loading.gif',
                    image: 'http://image.tmdb.org/t/p/w780/${moviePreview.imageUrl}'),
              ),
            ),
          );
  }
}
