import 'dart:async';
import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/models/movie_details.dart';
import 'package:flutter_application_1/services/movie.dart';
import 'package:flutter_application_1/utils/constants.dart';
import 'package:flutter_application_1/widgets/custom_loading_spin_kit_ring.dart';
import 'package:flutter_application_1/utils/star_calculator.dart'
    as starCalculator;
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_application_1/secret/the_moviedb_api.dart' as secret;

class DetailsScreen extends StatefulWidget {
  late int id;
  //final Color themeColor;
  DetailsScreen({required this.id});
  @override
  _DetailsScreenState createState() => _DetailsScreenState();

  Future<MovieDetails> getMovieDetails() async {
    MovieModel movieModel = MovieModel();
    MovieDetails temp = await movieModel.getMovieDetails(movieID: id);
    return temp;
  }

  Future<String> getMovieVideo() async {
    var response = await http.get(Uri.parse(
        "https://api.themoviedb.org/3/movie/${id}/videos?api_key=${secret.themoviedbApi}"));

    var data = json.decode(response.body);
    //print("result datadata ${data}");
    final key = [];
    for (var item in data["results"]) {
      key.add(item['key']);
    }
    print("result item ${key[0]}");

    return key[0];
    // if (response.statusCode == 200) {
    //   //print(json.decode(response.body));
    //   return Trailor.fromJson(json.decode(response.body));
    // } else {
    //   throw Exception('not able to Fetch the trening Movies');
    // }
  }
}

class _DetailsScreenState extends State<DetailsScreen> {
  MovieDetails? movieDetails;
  List<Widget>? stars;
  late YoutubePlayerController _controller;

  @override
  void initState() {
    super.initState();
    () async {
      MovieDetails temp = await widget.getMovieDetails();
      String video = await widget.getMovieVideo();
      List<Widget> temp2 =
          starCalculator.getStars(rating: temp.rating, starSize: 20.0);
      print('video ${video}');
      setState(() {
        movieDetails = temp;
        stars = temp2;
      });
      _controller = YoutubePlayerController(
        initialVideoId: video,
        flags: YoutubePlayerFlags(
          autoPlay: true,
          mute: true,
        ),
      );
    }();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF141C35),
      body: (stars == null)
          ? CustomLoadingSpinKitRing(loadingColor: Color(0x4D000000))
          : CustomScrollView(
              slivers: [
                SliverAppBar(
                  shadowColor: Colors.transparent.withOpacity(0.1),
                  elevation: 0,
                  backgroundColor: kPrimaryColor,
                  leading: Padding(
                    padding: EdgeInsets.only(left: 3.0),
                    child: IconButton(
                      icon: Icon(Icons.arrow_back_ios),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                  automaticallyImplyLeading: false,
                  pinned: true,
                  snap: false,
                  floating: false,
                  expandedHeight: 230,
                  flexibleSpace: FlexibleSpaceBar(
                    centerTitle: true,
                    title: Text(
                      kDetailsScreenTitleText,
                      style: TextStyle(fontSize: 20.0, color: Colors.white),
                    ),
                    background: SafeArea(
                      child: CachedNetworkImage(
                        fit: BoxFit.cover,
                        placeholder: (context, url) => SafeArea(
                          child: Container(
                            height: 200,
                            child: CustomLoadingSpinKitRing(
                                loadingColor: Colors.grey),
                          ),
                        ),
                        imageUrl: movieDetails!.backgroundURL,
                        errorWidget: (context, url, error) => SafeArea(
                          child: Container(
                            height: 220,
                            child: CustomLoadingSpinKitRing(
                                loadingColor: Colors.grey),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SliverFillRemaining(
                  child: Column(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: Wrap(
                              children: [
                                Text(
                                  "${movieDetails!.title} ",
                                  style: TextStyle(
                                      fontSize: 30.0,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                ),
                                Text(
                                  (movieDetails!.year == "")
                                      ? ""
                                      : "(${movieDetails!.year})",
                                  style: TextStyle(
                                      fontSize: 30, color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 10),
                          Row(
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20),
                                child: Row(children: stars!),
                              ),
                              Align(
                                alignment: Alignment.topRight,
                                child: ElevatedButton(
                                  onPressed: () {
                                    showDialog(
                                      context: context,
                                      builder: (context) {
                                        return Dialog(
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(40)),
                                          elevation: 10,
                                          child: Container(
                                            child: ListView(
                                              shrinkWrap: true,
                                              children: <Widget>[
                                                YoutubePlayerBuilder(
                                                  player: YoutubePlayer(
                                                    controller: _controller,
                                                    showVideoProgressIndicator:
                                                        true,
                                                    progressIndicatorColor:
                                                        Colors.amber,
                                                    progressColors:
                                                        ProgressBarColors(
                                                      playedColor: Colors.amber,
                                                      handleColor:
                                                          Colors.amberAccent,
                                                    ),
                                                    onReady: () {
                                                      _controller
                                                          .addListener(() {});
                                                    },
                                                  ),
                                                  builder: (context, player) =>
                                                      player,
                                                ),
                                              ],
                                            ),
                                          ),
                                        );
                                      },
                                    );
                                  },
                                  child: Icon(
                                    Icons.play_arrow,
                                    color: Colors.red,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 30),
                          SingleChildScrollView(
                            physics: BouncingScrollPhysics(),
                            scrollDirection: Axis.horizontal,
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: Row(
                                  children:
                                      movieDetails!.getGenresList(context)),
                            ),
                          ),
                          SizedBox(height: 10),
                        ],
                      ),
                      if (movieDetails!.overview != "")
                        Expanded(
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 20, vertical: 30),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(
                                    right: 10,
                                    left: 10,
                                    bottom: 10,
                                  ),
                                  child: Container(
                                    child: Text(kStoryLineTitleText,
                                        style: TextStyle(
                                            fontSize: 25.0,
                                            color: Colors.white)),
                                  ),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                        right: 10,
                                        left: 10,
                                        top: 10,
                                        bottom: 40),
                                    child: Text(
                                      movieDetails!.overview,
                                      style: TextStyle(
                                          fontSize: 20.0,
                                          color: Color(0xFFC9C9C9)),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                    ],
                  ),
                )
              ],
            ),
    );
  }
}
