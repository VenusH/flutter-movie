import 'package:flutter/material.dart';
import 'package:flutter_application_1/utils/file_manager.dart' as file;
import 'package:flutter_application_1/utils/scroll_top_with_controller.dart'
    as scrollTop;
import 'package:flutter_application_1/services/movie.dart';
import 'package:flutter_application_1/widgets/custom_loading_spin_kit_ring.dart';
import 'package:flutter_application_1/widgets/movie_card.dart';
import 'package:flutter_application_1/widgets/movie_card_container.dart';
import 'package:flutter_application_1/widgets/shadowless_floating_button.dart';

class GenreWiseScreen extends StatefulWidget {
  final String genre;
  final int genreId;
  GenreWiseScreen({Key? key, required this.genre, required this.genreId})
      : super(key: key);

  @override
  State<GenreWiseScreen> createState() => _GenreWiseScreenState();
}

class _GenreWiseScreenState extends State<GenreWiseScreen>
    with TickerProviderStateMixin {
  Color? themeColor;
  List<MovieCard>? _movieCards;
  bool showBackToTopButton = false;
  ScrollController? _scrollController;
  Future<void> loadData() async {
    MovieModel movieModel = MovieModel();
    _movieCards = await movieModel.getGenreWiseMovies(
        genreId: widget.genreId, themeColor: themeColor!);

    setState(() {
      scrollTop.scrollToTop(_scrollController!);
      showBackToTopButton = false;
    });
  }

  @override
  void initState() {
    super.initState();
    () async {
      themeColor = await file.currentTheme();
      print(themeColor);
      _scrollController = ScrollController()
        ..addListener(() {
          setState(() {
            showBackToTopButton = (_scrollController!.offset >= 200);
          });
        });
      setState(() {
        loadData();
      });
    }();
  }

  @override
  void dispose() {
    if (_scrollController != null) _scrollController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF141C35),
      appBar: AppBar(
        toolbarHeight: 50,
        shadowColor: Colors.transparent.withOpacity(0.1),
        elevation: 0,
        backgroundColor: Color(0xFF161616),
        title: Text(
            "${widget.genre.endsWith('y') ? widget.genre.replaceRange(widget.genre.length - 1, widget.genre.length, 'ie') : widget.genre}s",
            style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
            )),
      ),
      body: (_movieCards == null)
          ? CustomLoadingSpinKitRing(loadingColor: themeColor)
          : (_movieCards!.length == 0)
              ? Center(child: Text("Movies not found"))
              : MovieCardContainer(
                  scrollController: _scrollController!,
                  themeColor: themeColor!,
                  movieCards: _movieCards!,
                ),
      floatingActionButton: showBackToTopButton
          ? ShadowlessFloatingButton(
              iconData: Icons.keyboard_arrow_up_outlined,
              onPressed: () {
                setState(() {
                  scrollTop.scrollToTop(_scrollController!);
                });
              },
              backgroundColor: themeColor,
            )
          : null,
    );
  }
}
