import 'package:flutter/material.dart';

import '../components/trending_movies.dart';
import '../components/top_rated.dart';
import '../components/upcoming_movies.dart';


class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: 15.0),
            Text(
              'Trending Movies',
              style: TextStyle(fontSize: 20.0, color: Colors.white, fontWeight: FontWeight.bold),
            ),
            TrendingMovies(),
            SizedBox(height: 30.0),
            Text(
              'Top Rated Movies',
               style: TextStyle(fontSize: 20.0, color: Colors.white, fontWeight: FontWeight.bold),

            ),
            SizedBox(height: 10.0),
            TopRated(),
            SizedBox(height: 30.0),
            Text(
              'Upcoming Movies',
              style: TextStyle(fontSize: 20.0, color: Colors.white, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10.0),
            UpcomingMovies(),
          ],
        ),
      ),
    );
  }
}