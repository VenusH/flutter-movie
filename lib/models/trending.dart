class Trending {
  final List movies;
  Trending({required this.movies});

  factory Trending.fromJson(Map<String, dynamic> json) {
    return Trending(
      movies: json['results'],
    );
  }
}


class VideoApp {
  final List movies;
  VideoApp({required this.movies});

  factory VideoApp.fromJson(Map<String, dynamic> json) {
    return VideoApp(
      movies: json['results'],
    );
  }
}