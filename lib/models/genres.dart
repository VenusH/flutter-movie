class Genres {
  final List genres_movies;
  Genres({required this.genres_movies});

  factory Genres.fromJson(Map<String, dynamic> json) {
    return Genres(
      genres_movies: json['genres'],
    );
  }
}

class FilterGenres {
  final List filter_genres;
  FilterGenres({required this.filter_genres});

  factory FilterGenres.fromJson(Map<String, dynamic> json) {
    return FilterGenres(
      filter_genres: json['results'],
    );
  }
}